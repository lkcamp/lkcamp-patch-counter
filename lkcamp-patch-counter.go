package main

import (
	"os"
	"fmt"
	"regexp"
	"os/exec"
	"gitlab.com/lucmaga/lkcamp-patch-counter/mail-parser"
)

func wasPushed(s string) bool {
	// Search only commits made after lkcamp was created
	args := []string{"--git-dir=kernel/.git", "--no-pager", "log", "--oneline", "--since=03-09-2018", "--single-worktree"}
	args = append(args, fmt.Sprintf("--grep=%s", s))
	cmd := exec.Command("git", args...)
	out,err := cmd.Output()
	if err != nil {
		fmt.Println(err)
		return false
	}
	if len(out) == 0 {
		return false
	}
	return true
}

func isPatch() func( string) string {
	// Get patches with the [Lkcamp] tag
	re := regexp.MustCompile(`.*\[Lkcamp\] \[PATCH.*] (.*)$`)
	x := func(s string) string {
			match := re.FindStringSubmatch(s)
			if len(match) >= 2 {
				return match[1]
			}
			return ""
		}
	return x
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage: lkcamp-patch-counter <mail-file>")
		os.Exit(1)
	}
	f, err := os.Open(os.Args[1])
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	comp := isPatch()
	get := mailparser.GetMail(f)
	s := make(map[string]bool)
	for m,b := get(); b != false; m,b = get() {
		patchName := comp(m.Head.Subject)
		if patchName != "" {
			s[patchName] = false
		}
	}

	count := 0
	for k,_ := range s {
		if wasPushed(k) == true {
			count++
		}
	}
	fmt.Printf("Found %d patches submitted\n", count)
}

