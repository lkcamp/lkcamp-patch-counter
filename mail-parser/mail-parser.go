package mailparser

import (
	"io"
	"bufio"
	"regexp"
)

type Header struct {
	Raw string
	From string
	Date string
	Subject string
	MessageID string
}

type Mail struct {
	Head Header
	Body string
}

func parseHeader(s string)(Header, string) {
	/* I will try to make a single regex to get all stuff I need */
	/* TODO: Yeah I know it will not correctly get all subject
	 * but it's enought for instance.
	 */
	re := regexp.MustCompile(`([\s\S]*)From: (.*)\n([\s\S]*)Date: (.*)\n([\s\S]*)Subject: (.*)\n([\s\S]*)Message-ID: (.*)\n([\s\S]*)$`)
	match := re.FindAllStringSubmatch(s,12)
	header := Header{}
	header.From = match[0][2]
	header.Date = match[0][4]
	header.Subject = match[0][6]
	header.MessageID = match[0][8]
	return header, match[0][9]
}

func ParseMail(s string) Mail{
	/* Separate header form message */
	header, message := parseHeader(s);
	mail := Mail{Head: header, Body: message}
	return mail
}

func GetMail(r io.Reader) func()(Mail, bool) {
	scanner := bufio.NewScanner(r)
	spliter := func (data []byte, atEOF bool)(advance int, token []byte, err error) {
		if len(data) < 8 {
			return 0, data, bufio.ErrFinalToken
		}
		for i := 0; i < len(data) - 7; i++ {
			if string(data[i:i+7]) == "\n\n\nFrom" {
				return i+3, data[:i+2], nil
			}
		}
		if atEOF == true {
			return 0, data, bufio.ErrFinalToken
		}
		return 0, nil, nil
	}
	scanner.Split(spliter)
	x := func()(Mail, bool) {
		m := Mail{}
		if !scanner.Scan() {
			return m, false
		}
		m = ParseMail(scanner.Text())
		return m, true
	}
	return x
}
